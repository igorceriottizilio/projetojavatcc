package Entity;

import java.io.Serializable;

/**
 *
 * @author igorc
 */
public class Aluno implements Serializable{
    
    private String nome;
    private String numeroMatricula;
    private String email;
    private String telefone;
    private Projeto projeto;

    public Aluno(String nome, String numeroMatricula, String email, String telefone) {
        this.nome = nome;
        this.numeroMatricula = numeroMatricula;
        this.email = email;
        this.telefone = telefone;
    }
    
    public void setAluno(Aluno aluno){
        this.email = aluno.email;
        this.nome = aluno.nome;
        this.numeroMatricula = aluno.numeroMatricula;
        this.telefone = aluno.telefone;
        this.projeto = aluno.projeto;
    }
            
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumeroMatricula() {
        return numeroMatricula;
    }

    public void setNumeroMatricula(String numeroMatricula) {
        this.numeroMatricula = numeroMatricula;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }
    
    
}
