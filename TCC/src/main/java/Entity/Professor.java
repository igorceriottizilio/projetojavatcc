
package Entity;

import Enum.AreaInteresse;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author igorc
 */
public class Professor implements Serializable{
    private String nome;
    private String email;
    private String sugestaoProjeto;
    private AreaInteresse areaInteresse;
    private List<Projeto> projetosOrientador;
    private List<Projeto> projetosAvaliador;

    public Professor(String nome, String email, String sugestaoProjeto, AreaInteresse areaInteresse) {
        this.nome = nome;
        this.email = email;
        this.sugestaoProjeto = sugestaoProjeto;
        this.areaInteresse = areaInteresse;
        this.projetosAvaliador = new ArrayList<>();
        this.projetosOrientador = new ArrayList<>();
    }
    
    public void setProfessor(Professor professor){
        this.nome = professor.getNome();
        this.email = professor.getEmail();
        this.areaInteresse = professor.getAreaInteresse();
        this.sugestaoProjeto = professor.getSugestaoProjeto();
        this.projetosAvaliador = professor.getProjetosAvaliador();
        this.projetosOrientador = professor.getProjetosOrientador();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSugestaoProjeto() {
        return sugestaoProjeto;
    }

    public void setSugestaoProjeto(String sugestaoProjeto) {
        this.sugestaoProjeto = sugestaoProjeto;
    }

    public AreaInteresse getAreaInteresse() {
        return areaInteresse;
    }

    public void setAreaInteresse(AreaInteresse areaInteresse) {
        this.areaInteresse = areaInteresse;
    }

    public List<Projeto> getProjetosOrientador() {
        return projetosOrientador;
    }

    public void setProjetosOrientador(Projeto projetoOrientador) {
        this.projetosOrientador.add(projetoOrientador);
    }

    public List<Projeto> getProjetosAvaliador() {
        return projetosAvaliador;
    }

    public void setProjetosAvaliador(Projeto projetoAvaliador) {
        this.projetosAvaliador.add(projetoAvaliador);
    }
    
    
}
