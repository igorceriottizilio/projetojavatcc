
package Entity;

import java.io.Serializable;

/**
 *
 * @author igorc
 */
public class Projeto implements Serializable{
    private String titulo;
    private String descricao;
    private Aluno autor;
    private Professor orientador;

    public Projeto(String titulo, String descricao, Aluno autor, Professor orientador) {
        this.titulo = titulo;
        this.descricao = descricao;
        this.autor = autor;
        this.orientador = orientador;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Aluno getAutor() {
        return autor;
    }

    public void setAutor(Aluno autor) {
        this.autor = autor;
    }

    public Professor getOrientador() {
        return orientador;
    }

    public void setOrientador(Professor orientador) {
        this.orientador = orientador;
    }
    
    
}
