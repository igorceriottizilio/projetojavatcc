package View;

import Controller.Rendering;
import Exception.EmptyFieldException;
import javax.swing.JOptionPane;

/**
 *
 * @author igorc
 */
public class HomePage extends Controller.Controller {

    public static void display() {
        boolean ok = true;
        do {
            try {
                String option = reader("Digite opção da operação desejada:"
                        + "\n1 - Inserir Aluno"
                        + "\n2 - Inserir Professor");
                Rendering.directTo(option);

            } catch (EmptyFieldException ex) {
                JOptionPane.showMessageDialog(null, "Por favor, informe uma operação!");

            } catch (NullPointerException ex) {
                JOptionPane.showMessageDialog(null, "Encerrando o programa ...");
                System.exit(0);
            }

        } while (ok);
    }

}
