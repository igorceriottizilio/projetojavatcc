/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.Run;
import DAO.AlunoDAO;
import Entity.Aluno;
import Exception.EmptyFieldException;
import java.util.LinkedHashMap;
import javax.swing.JOptionPane;

/**
 *
 * @author igorc
 */
public class InsertAluno implements Run {
    
        LinkedHashMap<String, String> attributes;
        
        private String nome;
        private String email;
        private String telefone;
        private String numeroMatricula;

        public InsertAluno(){
           
           this.attributes = new LinkedHashMap<String, String>()
                {{
                     put("nome", nome);
                     put("email", email);
                     put("telefone", telefone);
                     put("numero da matricula", numeroMatricula);
                }};
        }
    
    public void run(){
        AlunoDAO alunoDAO = AlunoDAO.getInstance();
        
        attributes.forEach((key, value) -> addInfos(key, value));
        
        Aluno aluno = new Aluno(nome, numeroMatricula, email, telefone);
        alunoDAO.insert(aluno);
        HomePage.display();
    }
    
    public void addInfos(String key, String value){
        boolean ok = false;
        do {
            try {
                value = JOptionPane.showInputDialog(null, "Informe " + key + " do aluno");
                if (value.isEmpty()) {
                    throw new EmptyFieldException("Um aluno precisa ter " + key);
                }
                
                if(key.contains("nome")){
                    this.nome =  value;
                } else if (key.contains("telefone")){
                    this.telefone = value;
                } else if (key.contains("email")){
                    this.email = value;
                } else {
                    this.numeroMatricula = value;
                }
                
                ok = true;
            } catch (EmptyFieldException ex) {
                JOptionPane.showMessageDialog(null, ex);
                ok = false;
            } catch (NullPointerException npe) {
                HomePage.display();
            }
        } while (ok != true);
    }
}
