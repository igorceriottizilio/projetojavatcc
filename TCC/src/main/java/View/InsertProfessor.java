package View;

import Controller.Run;
import DAO.ProfessorDAO;
import Entity.Professor;
import Enum.AreaInteresse;
import Exception.EmptyFieldException;
import java.util.LinkedHashMap;
import javax.swing.JOptionPane;

/**
 *
 * @author igorc
 */
public class InsertProfessor implements Run {

    LinkedHashMap<String, String> attributes;

    private String nome;
    private String email;
    private String sugestaoProjeto;
    private AreaInteresse areaInteresse;

    public InsertProfessor() {

        this.attributes = new LinkedHashMap<String, String>() {
            {
                put("nome", nome);
                put("email", email);
                put("areaInteresse", "");
                put("sugestão de projeto", sugestaoProjeto);
            }
        };
    }

    public void run() {
        ProfessorDAO professorDao = ProfessorDAO.getInstance();

        attributes.forEach((key, value) -> addInfos(key, value));

        Professor professor = new Professor(nome, email, sugestaoProjeto, areaInteresse);
        professorDao.insert(professor);
        HomePage.display();
    }

    public void addInfos(String key, String value) {
        boolean ok = false;
        boolean isArea = key.equals("areaInteresse");

        if (!isArea) {
            do {
                try {
                    value = JOptionPane.showInputDialog(null, "Informe " + key + " do professor");
                    if (value.isEmpty()) {
                        throw new EmptyFieldException("Um professor precisa ter " + key);
                    }

                    if (key.contains("nome")) {
                        this.nome = value;
                    } else if (key.contains("projeto")) {
                        this.sugestaoProjeto = value;
                    } else if (key.contains("email")) {
                        this.email = value;
                    }

                    ok = true;
                } catch (EmptyFieldException ex) {
                    JOptionPane.showMessageDialog(null, ex);
                    ok = false;
                } catch (NullPointerException npe) {
                    HomePage.display();
                }
            } while (ok != true);

        } else {
            do {
                Object[] dados = AreaInteresse.values();
                areaInteresse = (AreaInteresse) JOptionPane.showInputDialog(
                        null,
                        "Área de Interesse:\n",
                        "Escolha a área de interesse",
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        dados,
                        AreaInteresse.DESENVOLVIMENTO_DE_SOFTWARE);
                ok = true;

                if (areaInteresse == null) {
                    ok = false;
                }
            } while (!ok);
        }
    }
}
