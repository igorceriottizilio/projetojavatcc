package View;

import Controller.Run;
import DAO.ProfessorDAO;
import Entity.Professor;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author igorc
 */
public class ListPropostas implements Run {

    private DefaultTableModel modelo = new DefaultTableModel();
    private JTable tabela;

    @Override
    public void run() {
        ProfessorDAO professorDAO = ProfessorDAO.getInstance();

        tabela = new JTable(modelo);
        modelo.addColumn("Professor");
        modelo.addColumn("Área de Interesse");
        modelo.addColumn("Sugestão de Projeto");
        modelo.addColumn("Email");
        tabela.getColumnModel().getColumn(0)
                .setPreferredWidth(10);
        tabela.getColumnModel().getColumn(1)
                .setPreferredWidth(120);
        tabela.getColumnModel().getColumn(1)
                .setPreferredWidth(80);
        tabela.getColumnModel().getColumn(1)
                .setPreferredWidth(120);

        for (Object professor : professorDAO.getCollection()) {
            Professor getProfessor = (Professor) professor;

            modelo.setNumRows(0);
            modelo.addRow(new Object[]{getProfessor.getNome(), getProfessor.getAreaInteresse(),
                getProfessor.getSugestaoProjeto(), getProfessor.getEmail()});
        }

        tabela.setVisible(true);
    }

}
