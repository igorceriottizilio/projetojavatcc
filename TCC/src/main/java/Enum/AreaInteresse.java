
package Enum;

import java.io.Serializable;

/**
 *
 * @author igorc
 */
public enum AreaInteresse implements Serializable{
    ARQUITETURA_COMPUTACIONAL, DESENVOLVIMENTO_DE_SOFTWARE, BIOQUIMICA, SAUDE_PUBLICA;
}
