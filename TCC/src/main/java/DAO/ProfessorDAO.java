package DAO;

import Entity.Aluno;
import Entity.Professor;
import Interface.DaoInterface;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author igorc
 */
public class ProfessorDAO implements DaoInterface{
    private ArrayList collection;
    private static ProfessorDAO professores = null;
    
    public ProfessorDAO(ArrayList collection){
        this.collection = collection;
        professores = this;
    }
    
    public static ProfessorDAO getInstance() {
        return professores;
    }
            
    @Override
    public void insert(Object obj) {
        getCollection().add(obj);
    }

    @Override
    public void delete(Object obj) {
        getCollection().remove(obj);
    }

    @Override
    public void edit(Object newObj) throws ClassCastException{
        Professor newProfessor = (Professor) newObj;
        Professor oldProfessor = searchProfessorByNome(newProfessor.getNome());
        oldProfessor.setProfessor(newProfessor);
    }

    @Override
    public ArrayList search(Object key) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList getCollection() {
        return collection;
    }
    
    public Professor searchProfessorByNome(String nome){
        List<Professor> tempProfessores = new ArrayList<>();
        
        for(Object professor : getCollection()){
            tempProfessores.add((Professor) professor);
        }
  
        return tempProfessores.stream().filter(professor -> professor.getNome().equals(nome)).findFirst().orElse(null);
            
    }
    
}
