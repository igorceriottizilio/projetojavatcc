package DAO;

import Entity.Aluno;
import Interface.DaoInterface;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author igorc
 */
public class AlunoDAO implements DaoInterface{
    private ArrayList collection;
    private static AlunoDAO alunos = null;
    
    public AlunoDAO(ArrayList collection){
        this.collection = collection;
        alunos = this;
    }
    
    public static AlunoDAO getInstance() {
        return alunos;
    }
            
    @Override
    public void insert(Object obj) {
        getCollection().add(obj);
    }

    @Override
    public void delete(Object obj) {
        getCollection().remove(obj);
    }

    @Override
    public void edit(Object newObj) throws ClassCastException{
        Aluno newAluno = (Aluno) newObj;
        Aluno oldAluno = searchAlunoByNome(newAluno.getNome());
        oldAluno.setAluno(newAluno);
    }

    @Override
    public ArrayList search(Object key) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList getCollection() {
        return collection;
    }
    
    public Aluno searchAlunoByNome(String nome){
        List<Aluno> tempAlunos = new ArrayList<>();
        
        for(Object aluno : getCollection()){
            tempAlunos.add((Aluno) aluno);
        }
  
        return tempAlunos.stream().filter(aluno -> aluno.getNome().equals(nome)).findFirst().orElse(null);
            
    }
    
}
