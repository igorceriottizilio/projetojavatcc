
package Interface;

import java.util.ArrayList;

/**
 *
 * @author igorc
 */
public interface DaoInterface {
    public void insert (Object obj);
    
    public void delete (Object obj);
    
    public void edit (Object newObj);
    
    public ArrayList search (Object key);
}
