/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author igorc
 */
public class FileManager {
    
    public static boolean fileExists(String fileName){
        File file = new File(fileName);
        return (file.exists());
    }
    
     public static void createFile(String fileName) throws IOException{
        if (!fileExists(fileName)){
            File file = new File(fileName);
            file.createNewFile();
        }
    }
     
     public static void deleteFile(String fileName) throws IOException {
        if (fileExists(fileName)){
            File file = new File(fileName);
            file.delete();
        }
    }
     
     public static Object openFile(String fileName) throws IOException, ClassNotFoundException {
        Object obj = null;
        File f = new File(fileName);
        if (f.exists()) {
            FileInputStream in = new FileInputStream(f);
            ObjectInputStream sin = new ObjectInputStream(in);
            obj = sin.readObject();
            in.close();
        } else {
            System.out.println("File not found");
        }
        return obj;
    }
     
     public static void saveFile(Object obj, String fileName) throws IOException{
        File f = new File(fileName);
        FileOutputStream out = new FileOutputStream(f);
        ObjectOutputStream sout = new ObjectOutputStream(out);
        sout.writeObject(obj);
        out.close();
    }
}
