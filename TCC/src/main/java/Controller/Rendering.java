/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author igorc
 */
public class Rendering {
    private static HashMap options;

    static {
        Rendering.options = new HashMap<String, String>() {{
                put("1", "View.InsertAluno");
                put("2", "View.InsertProfessor");
                put("3", "View.ListPropostas");
        }};
    }

    public static void directTo(String cmd) {
        String actionClass = (String) options.get(cmd);
        try {
            Class classe = Class.forName(actionClass);
            Run run = (Run) classe.newInstance();
            run.run();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Rendering.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException iex) {
            Logger.getLogger(Rendering.class.getName()).log(Level.SEVERE, null, iex);
        } catch (IllegalAccessException iaex) {
            Logger.getLogger(Rendering.class.getName()).log(Level.SEVERE, null, iaex);
        }

    }
}
